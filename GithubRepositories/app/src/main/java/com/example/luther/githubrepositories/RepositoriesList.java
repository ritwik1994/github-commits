package com.example.luther.githubrepositories;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.luther.githubrepositories.adapters.CustomAdapterRepositoriesList;
import com.example.luther.githubrepositories.data.DataContract;
import com.example.luther.githubrepositories.data.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class RepositoriesList extends AppCompatActivity implements AbsListView.OnScrollListener{

    private RelativeLayout bottomLoadMoreLayout;
    private ListView gitDataListView;
    private int currentFirstVisibleItem = 0;
    private int currentVisibleItemCount = 0;
    private int totalItemCount = 0;
    private int currentScrollState = 0;
    private View progressView;
    private boolean loadingMore = false;
    private int offset = 1;
    private DatabaseHelper mDataHelper;
    private boolean completeResultsAfterScrollingAtTheEnd = false;
    private CustomAdapterRepositoriesList customCursorAdapter;
    Cursor cursorForExtractingRepositories;
    String nameToDisplay="",userUuid="",userName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repositories_list);
        mDataHelper = DatabaseHelper.getInstance(this);

        gitDataListView = (ListView)findViewById(R.id.listview_githubdata);
        bottomLoadMoreLayout = (RelativeLayout) findViewById(R.id.loadItemsLayout_listView);
        progressView=(View)findViewById(R.id.repositoriesLoadProgress);

        //For showing progress bar
        showProgress(true);
        bottomLoadMoreLayout.setVisibility(View.INVISIBLE);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.containsKey("username"))
                userName = extras.getString("username");
            if(extras.containsKey("uuidOfUser"))
                userUuid = extras.getString("uuidOfUser");
            if(extras.containsKey("nameOfTheUser"))
                nameToDisplay = extras.getString("nameOfTheUser");
        }

        if(!(nameToDisplay.equals(""))){
            setTitle(nameToDisplay+"'s Git");
        }else{
            setTitle("Git Repository");
        }

        //Displaying data from local database.
        cursorForExtractingRepositories = mDataHelper.getReadableDatabase().query(
                DataContract.GitDetails.TABLE_NAME,  // Table to Query
                null, // all columns
                DataContract.GitDetails.USER_UUID + "=" + "'" + userUuid + "'", // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );
        customCursorAdapter = new CustomAdapterRepositoriesList(this, cursorForExtractingRepositories);
        gitDataListView.setAdapter(customCursorAdapter);
        if(cursorForExtractingRepositories.getCount() == 0) {
            LoadGitData loadGitData= new LoadGitData(offset,userName);
            loadGitData.execute();
        }else{
            showProgress(false);
        }
        gitDataListView.setOnScrollListener(this);

    }

    //Scrolling activity call

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }
    private void isScrollCompleted() {
        if(this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE && ((currentFirstVisibleItem + currentVisibleItemCount)>=totalItemCount))
        {
            if(!loadingMore)
            {
                loadingMore = true;
                offset +=1;
                if(completeResultsAfterScrollingAtTheEnd){
                }else {
                    LoadGitData loadGitData = new LoadGitData(offset,userName);
                    loadGitData.execute();
                    showLoadMoreProgress(true);
                }
            }
        }
    }
    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;
    }

    @Override
    public void onDestroy() {
        showProgress(false);
        if(bottomLoadMoreLayout.isShown())
            bottomLoadMoreLayout.setVisibility(View.INVISIBLE);
        super.onDestroy();
    }


    // For Loading up the data
    public class LoadGitData extends AsyncTask<String, Void, String> {

        HttpsURLConnection urlConnection = null;
        BufferedReader readerForInput = null;
        URL url = null;
        Boolean postexec = false;
        String userName;
        int offset = 1;

        //Storing up the data in local database.
        public void addToJSON(String response){
            try {
                JSONArray data = new JSONArray(response);
                if(data.length() < 15){
                    completeResultsAfterScrollingAtTheEnd = true;
                }
                if(data.length() > 0){
                    JSONObject objectForResponse;
                    for (int i = 0; i < data.length(); i++){
                        String uuidForReponse="",description="",repositoryName="",watchers="0",bugs="0",language="null",userId="";
                        objectForResponse = data.getJSONObject(i);
                        if(objectForResponse.has("id")){
                            uuidForReponse = objectForResponse.getString("id");
                        }
                        if(objectForResponse.has("owner")){
                            JSONObject ownerObject = new JSONObject(objectForResponse.getString("owner"));
                            if(ownerObject.has("id")){
                                userId=ownerObject.getString("id");
                            }
                        }
                        if(objectForResponse.has("description")){
                            description = objectForResponse.getString("description");
                        }
                        if(objectForResponse.has("name")){
                            repositoryName = objectForResponse.getString("name");
                        }
                        if(objectForResponse.has("watchers_count")){
                            watchers = objectForResponse.getString("watchers_count");;
                        }
                        if(objectForResponse.has("open_issues_count")){
                            bugs = objectForResponse.getString("open_issues_count");
                        }
                        if(objectForResponse.has("language")){
                            language = objectForResponse.getString("language");
                        }
                        SQLiteDatabase db = mDataHelper.getWritableDatabase();
                        ContentValues cv = new ContentValues();
                        cv.put(DataContract.GitDetails.REPOSITORY_UUID, uuidForReponse);
                        cv.put(DataContract.GitDetails.USER_UUID,userId);
                        cv.put(DataContract.GitDetails.BUGS, bugs);
                        cv.put(DataContract.GitDetails.DESCRIPTION, description);
                        cv.put(DataContract.GitDetails.WATCHERS, watchers);
                        cv.put(DataContract.GitDetails.LANGUAGE_USED, language);
                        cv.put(DataContract.GitDetails.REPOSITORY_NAME, repositoryName);
                        int value = (int)db.insertWithOnConflict(DataContract.GitDetails.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                    }
                }else{

                }
            }catch(Exception e){
                System.out.println(e);
            }
        }

        public LoadGitData( int offset,String userName) {
            this.offset = offset ;
            this.userName = userName;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("https://api.github.com/users/"+userName+"/repos?page=" + offset + "&per_page=15");
                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                System.out.println(urlConnection.getResponseCode());
                readerForInput = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringbuilderForResponse = new StringBuilder();
                String line = "";
                while ((line = readerForInput.readLine()) != null) {
                    stringbuilderForResponse.append(line);
                }
                readerForInput.close();
                addToJSON(stringbuilderForResponse.toString());
                postexec = true;
            }
            catch (Exception e) {
                System.out.println(e);
            }
            return null;}

        protected void onPostExecute(String result) {
            showProgress(false);
            if(bottomLoadMoreLayout.isShown())
                bottomLoadMoreLayout.setVisibility(View.INVISIBLE);
            if(postexec){
                if(bottomLoadMoreLayout.isShown())
                    bottomLoadMoreLayout.setVisibility(View.INVISIBLE);
                if (loadingMore) {
                    loadingMore = false;
                }
                Cursor cursorForDisplayingRepo;
                cursorForDisplayingRepo = mDataHelper.getReadableDatabase().query(
                        DataContract.GitDetails.TABLE_NAME,  // Table to Query
                        null, // all columns
                        DataContract.GitDetails.USER_UUID + "=" + "'" + userUuid + "'", // Columns for the "where" clause
                        null, // Values for the "where" clause
                        null, // columns to group by
                        null, // columns to filter by row groups
                        null // sort order
                );
                customCursorAdapter.swapCursor(cursorForDisplayingRepo);
                customCursorAdapter.notifyDataSetChanged();
            }else{
                showProgress(false);
                completeResultsAfterScrollingAtTheEnd = false;
                Toast.makeText(getBaseContext(),getString(R.string.lostConnectionString),Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showLoadMoreProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            bottomLoadMoreLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, whicsssssh allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

}
