package com.example.luther.githubrepositories;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.luther.githubrepositories.data.DataContract;
import com.example.luther.githubrepositories.data.DatabaseHelper;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    public TextInputLayout userNameTextLayout;
    public EditText userNameText;
    String uuidOfUser="";
    public Button repositoriesToLoadButton;
    public String nameOfTheUser="";
    private DatabaseHelper mDataHelper;
    private ProgressDialog dialogBoxForCheckingUser ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDataHelper = DatabaseHelper.getInstance(this);

        //Declaring all items.

        userNameTextLayout = (TextInputLayout)findViewById(R.id.userNameTextLayout);
        userNameText = (EditText)findViewById(R.id.userNameText);
        repositoriesToLoadButton = (Button)findViewById(R.id.repositoriesToLoadButton);

        repositoriesToLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                extractingDataFromTextView();
            }
        });

    }

    // Used for focussing error textfield.
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //Extracting data from textField.
    public void extractingDataFromTextView(){
        boolean cancel = false;
        String userName = userNameText.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            userNameTextLayout.setError(getString(R.string.errorMessageForEmptyField));
            requestFocus(userNameText);
            cancel = true;
        }else
        {
            userNameTextLayout.setErrorEnabled(false);
        }

        if(!cancel){
            dialogBoxForCheckingUser=new ProgressDialog(this);
            dialogBoxForCheckingUser.setMessage(getString(R.string.userValidation));
            dialogBoxForCheckingUser.show();
            CheckForUSerRegisteredInGithub checkForUSerRegisteredInGithub= new CheckForUSerRegisteredInGithub(userName);
            checkForUSerRegisteredInGithub.execute();
        }
    }

    public void dismissProgressDialog() {
        if (dialogBoxForCheckingUser != null && dialogBoxForCheckingUser.isShowing()) {
            dialogBoxForCheckingUser.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }


    // For checking up user in github
    public class CheckForUSerRegisteredInGithub extends AsyncTask<String, Void, String> {

        HttpsURLConnection urlConnection = null;
        URL url = null;
        Boolean postexec = false;
        String userName;
        Boolean userNotFound = false;

        //Storing up the data in local database.
        public void addToJSON(String response){
            try {
                JSONObject jsonDataForUser = new JSONObject(response);
                if(jsonDataForUser.has("id")){
                    uuidOfUser = Integer.toString(jsonDataForUser.getInt("id"));
                }
                if(jsonDataForUser.has("name")){
                    nameOfTheUser = jsonDataForUser.getString("name");
                }
                if(jsonDataForUser.has("message")){
                    if((jsonDataForUser.getString("message")).equals("Not Found")){
                        userNotFound = true;
                    }
                }
                if(!(uuidOfUser.equals(""))){
                    SQLiteDatabase db = mDataHelper.getWritableDatabase();
                    ContentValues cv = new ContentValues();
                    cv.put(DataContract.UserDetails.USER_UUID, uuidOfUser);
                    int value = (int)db.insertWithOnConflict(DataContract.UserDetails.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
                }
            }catch(Exception e){
                System.out.println(e);
            }

        }

        public CheckForUSerRegisteredInGithub( String userName) {
            this.userName = userName ;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                url = new URL("https://api.github.com/users/" + userName );
                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                System.out.println(urlConnection.getResponseCode());
                BufferedReader readerForInput;
                if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                    readerForInput = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                } else {
                    readerForInput = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                }
                StringBuilder stringbuilderForResponse = new StringBuilder();
                String line;
                while ((line = readerForInput.readLine()) != null) {
                    stringbuilderForResponse.append(line);
                }
                readerForInput.close();
                addToJSON(stringbuilderForResponse.toString());
                postexec = true;
            }
            catch (Exception e) {
                System.out.println(e);
            }
            return null;}

        protected void onPostExecute(String result) {
            if(dialogBoxForCheckingUser.isShowing())
                dialogBoxForCheckingUser.hide();
            if(postexec){
                if(userNotFound){
                    Toast.makeText(getBaseContext(),"Sorry! The user doesnot exist.",Toast.LENGTH_SHORT).show();
                }else{
                    if(!(uuidOfUser.equals(""))){
                        dialogBoxForCheckingUser.hide();
                        Intent githubRepositoryName = new Intent(MainActivity.this,RepositoriesList.class);
                        githubRepositoryName.putExtra("username", userName);
                        githubRepositoryName.putExtra("uuidOfUser",uuidOfUser);
                        githubRepositoryName.putExtra("nameOfTheUser",nameOfTheUser);
                        startActivity(githubRepositoryName);
                    }
                }
            }else{
                Toast.makeText(getBaseContext(),getString(R.string.lostConnectionString),Toast.LENGTH_SHORT).show();
            }
        }
    }
}
